var rootUrl = 'http://localhost:4567';

var buildJobName = 'first-jenkins-job'
var deployJobName = 'deploy-job';

var activeJobName = '';

$('#form-fields').load('../build-job.html');
activeJobName = buildJobName;
$('.build-job').prop('disabled', true);


var baseUrl = rootUrl+"/job/"+activeJobName;

var username = "";
var token = "";


var isBuilding = false;

$('#jobName').val(activeJobName);

var interval;

 

$('#loginForm').submit(function (event) {
  event.preventDefault();

  var isRememberMeChecked = $('input[name=rememberMe]').is(':checked');
  var $inputs = $('#loginForm :input');
  var values = {};
  $inputs.each(function () {
    values[this.name] = $(this).val();

  });

  if (values['username'] === "" || values['token'] === "") {
    alert('Username or Access Token cannot be empty');
  } else {
   
    $.ajax({
      type: "POST",
      headers: {
        "Authorization": makeBasicAuth(values['username'], values['token'])
      },
      url: rootUrl+'/view/all/newJob',
      success: function (response) {
        saveUsernameToken(values['username'], values['token']);
        window.location.replace("job.html");
    },
    error: function (jqXHR, exception) {
        alert("error");
    }
    });
  }


});

function buildJob(body){
  isBuilding = true;
  baseUrl = rootUrl+"/job/"+activeJobName;
  console.log(baseUrl);
  $('#buildSubmit').html("Building").attr("disabled", true);

$.ajax({
  type: 'POST',
  url: baseUrl+'/build',
  contentType: 'application/json',
  data: {
    "json": JSON.stringify(body),
  },
  headers: {  
      "Content-Type": "application/x-www-form-urlencoded",
      "Authorization": makeBasicAuth(getUsername(), getToken())
  },
  success: function(data){
      console.log(data);
      
      if (!$.trim(data)){  
        
        $('#consoleOutput').html("Waiting for the availible thread..");
        setTimeout(function() { 
          runLoop(); }, 5000);
    }
    else{   
      $('#buildSubmit').html("Build").attr("disabled", false);
        console.log("something went wrong data-> "+data);
    }
  }
});
}


$('#buildStop').on('click',function(e){
  $.ajax({
    type: 'POST',
    url: rootUrl+'/computer/(master)/executors/1/stop',
    headers: {  
        "Authorization": makeBasicAuth(getUsername(), getToken())
    },
    success: function(data){
      $('#buildSubmit').html("Build").attr("disabled", false);
    }
  });
});

function saveUsernameToken(username, token){
  localStorage.setItem('jenkins_user_name', username);
  localStorage.setItem('jenkins_user_token', token);
}

function getUsername(){
  if(localStorage.getItem('jenkins_user_name') != null){
    return localStorage.getItem('jenkins_user_name');
  }else{
    return username;
  }
}

function getToken(){
  if(localStorage.getItem('jenkins_user_token') != null){
    return localStorage.getItem('jenkins_user_token');
  }else{
    return token;
  }
}


function makeBasicAuth(user, password){
  var tok = user+":"+password;
  var hash = btoa(tok);
  return "Basic "+hash;
}


$('.logout').on('click', function (e) {
  e.preventDefault();
  localStorage.clear();
  window.location.replace("index.html");
})



$('#paramsForm').submit(function (e) {
  e.preventDefault();
  var $inputs = $('#paramsForm :input');
  var values = {};
  var body = new Object();
  body.parameter = [];

  $inputs.each(function () {
    if(this.name.length > 0){
      values[this.name] = $(this).val();
      var param = new Object();
      param.name = this.name;
      param.value = $(this).val();
      body.parameter.push(param);
    }
  });
buildJob(body);
});


function startConsoleOutput(){
  $.ajax({
    type: 'POST',
    url: baseUrl+'/lastBuild/consoleText/api',
    headers: {  
        "Authorization": makeBasicAuth(getUsername(), getToken())
    },
    success: function(data){
  
      $('#consoleOutput').html(data);
      
    }
  });
}

function runLoop(){
  interval  = setInterval(function() {
    startConsoleOutput();
       
  }, 5000);
}


$('.build-job').on('click', function(e){
  e.preventDefault();
  $('#consoleOutput').html("");
  $(this).prop('disabled', true);
  $('.deploy-job').prop('disabled', false);
  isBuilding = false;
  $('#buildSubmit').html("Build").attr("disabled", false);
  $('#form-fields').load('../build-job.html');
  activeJobName = buildJobName;
  $('#jobName').val(activeJobName);
  clearInterval(interval);

});

$('.deploy-job').on('click', function(e){
  e.preventDefault();
  $(this).prop('disabled', true);
  $('.build-job').prop('disabled', false);
  if(isBuilding){
    var image_tag = $("input[name=image_tag]").val();
    var REF_APP =  $("input[name=REF_APP]").val();
    var workflow_name =  $("input[name=workflow_name]").val();

    $('#consoleOutput').html("IMAGE_TAG:"+image_tag+"\
    REF_APP:"+REF_APP+"\
    workflow_name:"+workflow_name);
  }
  $('#buildSubmit').html("Build").attr("disabled", false);
  $('#form-fields').load('../deploy-job.html');
  activeJobName = deployJobName;
  $('#jobName').val(activeJobName);
  clearInterval(interval);

});
var $loading = $('#overlay');
$loading.hide();
$(document)
  .ajaxStart(function () {
    $loading.show();
  })
  .ajaxStop(function () {
    $loading.hide();
  });
